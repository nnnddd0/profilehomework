package com.example.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.profile.*

class profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://external-preview.redd.it/WIi7bB4S0bUTIcMbfSJ6y2-CZ8a5EG7PjAJgapiNGSk.jpg?auto=webp&s=fe850da6ab2c4967a94533f24513d3d279b163a2")
            .into(cover)

        Glide.with(this)
            .load("https://static.wikia.nocookie.net/shipping/images/2/25/Sasuke_Uchiha.png/revision/latest/scale-to-width-down/1000?cb=20160422160842")
            .into(profile)
    }
}